.PHONY: install test

DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SHELL=/bin/bash

ENV := $(DIR)/env/bin
PYTHON := $(ENV)/python
PIP := $(ENV)/pip
PEP8 := $(ENV)/pep8 --max-line-length=79
PYLINT := $(ENV)/pylint --rcfile=$(DIR)/.pylintrc
NOSETESTS := $(ENV)/nosetests

STATUS_ERROR := \033[1;31m*\033[0m Error
STATUS_OK := \033[1;32m*\033[0m OK


install-env:
	rm -rf "$(DIR)/env/" ;\
	virtualenv -p /usr/bin/python3.5 --clear "$(DIR)/env/" ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;

env-activate:
	. $(ENV)/activate

install-python-libs:
	$(PIP) install -U pip ;\
	$(PIP) install --no-cache-dir --upgrade -r "$(DIR)/requirements.txt" ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;

install: install-env env-activate install-python-libs


test-unittests:
	@mkdir -p ./tests/.reports/ ;\
	$(NOSETESTS) \
	    --with-coverage \
	    --cover-erase \
	    --cover-html --cover-html-dir="$(DIR)/tests/.reports/coverage/" \
	    --cover-package=./flask_settings ;\
	if [ $$? -eq 0 ]; then \
		echo -e "unittests: ... ${STATUS_OK}" ;\
	else \
		echo -e "unittests: ... ${STATUS_ERROR}" ;\
	fi;

test-pep8:
	@$(PEP8) "$(DIR)/flask_settings" "$(DIR)/tests" ;\
	if [ $$? -eq 0 ]; then \
		echo -e "pep8: ........ ${STATUS_OK}" ;\
	else \
		echo -e "pep8: ........ ${STATUS_ERROR}" ;\
	fi;

test-pylint:
	@$(PYLINT) "$(DIR)/flask_settings" "$(DIR)/tests" ;\
	if [ $$? -eq 0 ]; then \
		echo -e "pyint: ....... ${STATUS_OK}" ;\
	else \
		echo -e "pyint: ....... ${STATUS_ERROR}" ;\
	fi;

test: test-unittests test-pep8 test-pylint
