# -*- coding: utf-8 -*-
from __future__ import unicode_literals

try:
    import unittest2 as unittest
except ImportError:
    import unittest

from flask import Flask
from flask_settings import Settings


app = Flask(__name__)


class Test(unittest.TestCase):

    def test_default_config(self):
        settings = Settings(app, name='default')
        self.assertFalse(app.debug)
        self.assertFalse(app.testing)
        self.assertEqual(
            app.config['SQLALCHEMY_DATABASE_URI'],
            'postgresql://postgres:postgres@localhost:5432/default')
        self.assertEqual(
            settings.SQLALCHEMY_DATABASE_URI,
            'postgresql://postgres:postgres@localhost:5432/default')

    def test_development_config(self):
        settings = Settings(app, name='development')
        self.assertTrue(app.debug)
        self.assertFalse(app.testing)
        self.assertEqual(
            app.config['SQLALCHEMY_DATABASE_URI'],
            'postgresql://postgres:postgres@localhost:5432/development')
        self.assertEqual(
            settings.SQLALCHEMY_DATABASE_URI,
            'postgresql://postgres:postgres@localhost:5432/development')

    def test_testing_config(self):
        settings = Settings(app, name='testing')
        self.assertFalse(app.debug)
        self.assertTrue(app.testing)
        self.assertEqual(
            app.config['SQLALCHEMY_DATABASE_URI'],
            'postgresql://postgres:postgres@localhost:5432/testing')
        self.assertEqual(
            settings.SQLALCHEMY_DATABASE_URI,
            'postgresql://postgres:postgres@localhost:5432/testing')

    def test_production_config(self):
        settings = Settings(app, name='production')
        self.assertFalse(app.debug)
        self.assertFalse(app.testing)
        self.assertEqual(
            app.config['SQLALCHEMY_DATABASE_URI'],
            'postgresql://user:password@10.0.0.1:5432/production')
        self.assertEqual(
            settings.SQLALCHEMY_DATABASE_URI,
            'postgresql://user:password@10.0.0.1:5432/production')
