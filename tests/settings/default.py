# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask_settings import BasicConfig


class DefaultConfig(BasicConfig):
    SQLALCHEMY_DATABASE_PROTOCOL = 'postgresql'
    SQLALCHEMY_DATABASE_HOST = 'localhost'
    SQLALCHEMY_DATABASE_PORT = 5432
    SQLALCHEMY_DATABASE_USERNAME = 'postgres'
    SQLALCHEMY_DATABASE_PASSWORD = 'postgres'
    SQLALCHEMY_DATABASE_NAME = 'default'

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        # calculated constant
        return '{protocol}://{username}:{password}@{host}:{port}/{db}'.format(
            protocol=self.SQLALCHEMY_DATABASE_PROTOCOL,
            username=self.SQLALCHEMY_DATABASE_USERNAME,
            password=self.SQLALCHEMY_DATABASE_PASSWORD,
            host=self.SQLALCHEMY_DATABASE_HOST,
            port=self.SQLALCHEMY_DATABASE_PORT,
            db=self.SQLALCHEMY_DATABASE_NAME)
