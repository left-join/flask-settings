# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from tests.settings.default import DefaultConfig


class DevelopmentConfig(DefaultConfig):
    DEBUG = True

    SQLALCHEMY_DATABASE_NAME = 'development'
