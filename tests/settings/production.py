# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from tests.settings.default import DefaultConfig


class ProductionConfig(DefaultConfig):
    SQLALCHEMY_DATABASE_HOST = '10.0.0.1'
    SQLALCHEMY_DATABASE_USERNAME = 'user'
    SQLALCHEMY_DATABASE_PASSWORD = 'password'
    SQLALCHEMY_DATABASE_NAME = 'production'
