# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask_settings.basic import BasicConfig
from flask_settings.factory import Settings


__all__ = ['BasicConfig', 'Settings']
